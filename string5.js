function arrayJoin(value) {
    if (Array.isArray(value)) {
        let ans = value.join(' ')
        return ans;
    }
    else {
        return "Not a valid Input"
    }
}

module.exports = arrayJoin