function NamePrinter(value) {
    if (typeof value === 'object' && !Array.isArray(value)) {
        let name = ''
        for (let i in value) {
            let str = value[i].toLowerCase();
            let val = str[0].toUpperCase();
            let ans = val + str.substring(1)
            name = name + " " + ans
        }
        if (name.trim() === '') {
            return "Not Valid Object"
        }
        return name.trim()
    }
    else {
        return "Not Valid Object"
    }
}

module.exports = NamePrinter;