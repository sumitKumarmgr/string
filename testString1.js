let NumberExtractor = require("./string1")

console.log(NumberExtractor("$100.45"));
console.log(NumberExtractor("$1,002.22"));
console.log(NumberExtractor("-$123"));
console.log(NumberExtractor("- 100.45"));
console.log(NumberExtractor(["1002.22"]));
console.log(NumberExtractor(["-123"]));